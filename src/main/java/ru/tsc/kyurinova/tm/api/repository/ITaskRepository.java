package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);
}
