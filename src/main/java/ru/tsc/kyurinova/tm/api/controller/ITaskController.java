package ru.tsc.kyurinova.tm.api.controller;

import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void showTask(Task task);

    void clearTasks();

    void createTask();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
