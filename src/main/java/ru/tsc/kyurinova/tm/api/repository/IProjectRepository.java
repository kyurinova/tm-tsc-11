package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

}
